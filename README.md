# openvpn-ansible

## TODO
Stuff we can do to improve this project.

* improve README
* make templates/examples and scripts more dynamic


## Setup 

* ## Clone git repository
`` git clone git@bitbucket.org:nowmediatech/openvpn-ansible.git ``

OR

`` git clone https://%username%@bitbucket.org/nowmediatech/openvpn-ansible.git ``

 * ## Install and configure Ansible

`` sudo apt install ansible ``

#### Configure ansible hosts in /etc/ansible/hosts 

````
[openvpn]  
localhost ansible_become_pass='%password_sudo%'
````

#### Change variables in ansible_playbook.yml in project root directory

```` 
- name: Create a OpenVpn Server
       hosts: openvpn #Host group from /etc/ansible/hosts (ex: openvpn)
       become: yes
       become_user: root #Please add superuser username for installation (ex: root)
       remote_user: vfilatov #Please add os username for connection (ex: vfilatov)
       vars:
         privkey_password: "{{ lookup('password', '/tmp/privkey_passwordfile chars=ascii_letters') }}"
         project_path: /opt/openvpn-rsa #Path For Certification Authority Installation
         cert_main_path: vpnCA
         cert_path: vpnCA/certs
         crl_path: vpnCA/crls
         db_index: vpnCA/index.txt
         ca_pk_path: vpnCA/CAkey
         pk_path: vpnCA/Pkeys
         req_path: vpnCA/reqs
         openvpn_path: /etc/openvpn #Default Path For Openvpn
         openvpn_port: 1194
         openvpn_proto: udp
         openvpn_dev: tun
         openvpn_client_path: "{{ project_path }}/configs"
         update_apt_cache: true
         certs_count: 3 # Quantity of config files to generate
       tasks:
         - debug:
             msg: "project_path: {{ project_path }} cert_main_path: {{ cert_main_path }} cert_path: {{ cert_path }}"
         - debug:
             msg: "crl_path: {{ crl_path }} db_index: {{ db_index }} ca_pk_path: {{ ca_pk_path }}"
         - debug:
             msg: "pk_path: {{ pk_path }} req_path: {{ req_path }}"
       roles:
       - role: 'apt'
       - role: 'createcert'
         vars:
           privkey_pass_global: "{{ privkey_password }}"
       - role: 'openvpn'
````

#### Run ansible script for deploying.

``ansible-playbook -s ansible_playbook.yml``

##### Enjoy